from setuptools import setup, Extension, find_packages
import os

YDB_DIST = os.environ['ydb_dist']

setup(name = 'yottadb',
      version = '0.0.1',
      ext_modules = [Extension("_yottadb", sources = ['_yottadb.c'],
                               include_dirs=[YDB_DIST], library_dirs=[YDB_DIST],
                               extra_link_args= ["-l", "yottadb", "-l", "ffi"])],
      py_modules = ['yottadb'],
      packages=find_packages(include=['yottadb', 'yottadb.*']),
      package_data={'': ['_yottadb.pyi']},
      include_package_data=True,
      setup_requires=['pytest-runner'],
      tests_require=['pytest'],
      test_suite='test',
     )